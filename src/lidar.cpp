#include "Arduino.h"
#include <Stepper.h>
#define dirPin 2
#define stepPin 3
const int stepsPerRevolution = 200;

Stepper lidar_stepper(stepsPerRevolution, dirPin, stepPin);

void setup() {
   // set the speed at 60 rpm:
   lidar_stepper.setSpeed(5);
   // initialize the serial port:
   Serial.begin(9600);
}

void loop() {
   // step one revolution in one direction:
   Serial.println("clockwise");
   lidar_stepper.step(stepsPerRevolution);
   delay(500);
   // step one revolution in the other direction:
   Serial.println("counterclockwise");
   lidar_stepper.step(-stepsPerRevolution);
   delay(500);
}
